<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk;

use Closure;
use InvalidArgumentException;
use Jiwei\EasyHttpSdk\Middleware\Auth\AuthMiddlewareInterface;
use Jiwei\EasyHttpSdk\Middleware\Auth\JwtMiddleware;
use Jiwei\EasyHttpSdk\Policy\DefaultErrorHandlingPolicy;
use Jiwei\EasyHttpSdk\Policy\HandlingPolicyInterface;
use RuntimeException;

abstract class Option
{
    /** @var array<string, string> Api Host */
    const ENDPOINT_HOSTS = [
        'local' => '',
        'development' => '',
        'production' => ''
    ];

    /** @var int 授权缓存时间 */
    const AUTH_CACHE_EXPIRES_AT = 30000;

    /** @var string AuthMiddlewareInterface 鉴权中间件 */
    const AUTH_MIDDLEWARE = JwtMiddleware::class;

    /** @var string App Key 应用标志 */
    private $appId;
    /** @var string App Secret 应用密钥 */
    private $appSecret;
    /** @var string SDK 的 Stage 环境 */
    private $stage = "development";
    /** @var float 超时时间 */
    private $timeOut = 3.0;
    /** @var bool 调试模式 */
    private $debug = false;

    /**
     * @return Closure
     */
    abstract public function authorization(): Closure;

    /**
     * 错误处理策略
     * @return HandlingPolicyInterface
     */
    public function handlingPolicy(): HandlingPolicyInterface
    {
        return new DefaultErrorHandlingPolicy();
    }

    /**
     * @param string $appId
     * @return Option
     */
    public function setAppId(string $appId): self
    {
        $this->appId = $appId;
        return $this;
    }

    /**
     * @param string $appSecret
     * @return Option
     */
    public function setAppSecret(string $appSecret): self
    {
        $this->appSecret = $appSecret;
        return $this;
    }

    /**
     * @param string $stage
     * @return Option
     */
    public function setStage(string $stage): self
    {
        if(empty(static::ENDPOINT_HOSTS[$stage])){
            throw new InvalidArgumentException("set an undefine stage");
        }
        $this->stage = $stage;
        return $this;
    }

    /**
     * @param float $timeOut
     * @return Option
     */
    public function setTimeout(float $timeOut): self
    {
        $this->timeOut = $timeOut;
        return $this;
    }

    /**
     * @param bool $debug
     * @return Option
     */
    public function setDebug(bool $debug): self
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @return string
     */
    public function getAppSecret(): string
    {
        return $this->appSecret;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return static::ENDPOINT_HOSTS[$this->getStage()] ?? "";
    }

    /**
     * @return int
     */
    public function getAuthExpires(): int
    {
        return empty(static::AUTH_CACHE_EXPIRES_AT) ? self::AUTH_CACHE_EXPIRES_AT : static::AUTH_CACHE_EXPIRES_AT;
    }

    /**
     * @return string
     */
    public function getStage(): string
    {
        return $this->stage;
    }

    /**
     * @return float
     */
    public function getTimeOut(): float
    {
        return $this->timeOut;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @return string
     */
    public function getAuthorizationMiddleware(): string
    {
        $authorizationMiddleware = empty(static::AUTH_MIDDLEWARE) ? self::AUTH_MIDDLEWARE : static::AUTH_MIDDLEWARE;
        if (!class_exists($authorizationMiddleware)) {
            throw new RuntimeException("Auth Middleware is not exist");
        }
        if (!(new $authorizationMiddleware) instanceof AuthMiddlewareInterface) {
            throw new RuntimeException("Auth Middleware is not implements AuthMiddlewareInterface");
        }
        return $authorizationMiddleware;
    }
}
