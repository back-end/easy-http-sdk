<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\HandlerStack;
use InvalidArgumentException;
use Jiwei\EasyHttpSdk\Exception\ApplicationException;
use Jiwei\EasyHttpSdk\Exception\GuiltyResultException;
use Jiwei\EasyHttpSdk\Exception\SdkException;
use Jiwei\EasyHttpSdk\Exception\TimeOutExcetpion;
use Jiwei\EasyHttpSdk\Exception\UnknowResultException;
use Jiwei\EasyHttpSdk\Http\SdkRequest;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Cache\InvalidArgumentException as CacheInvalidArgumentException;
use RuntimeException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;


class Application implements ClientInterface
{

    private const TOKEN_CACHE_KEY = "Auth.%s";

    /** @var Option $option */
    private $option;

    /** @var null|LoggerInterface 日志记录器 */
    protected $logger = null;

    /** @var null|Client Guzzle客户端 */
    protected $client = null;

    /** @var null|CacheItemPoolInterface PS-6缓存 */
    protected $cache = null;

    /** @var array<string, mixed> 最后的一次请求调用上下文 */
    protected $lastRequestContext = [];

    /**
     * 初始化一个 SDK 应用，目前 config 中必填的有 app_key 和 app_secret, 剩余 stage 默认为 development, debug 默认为 false, timeout 默认为3.0,
     * 最后也可以传入 PSR 规范下的日志接口, 当然也可以不进行日志记录,
     * Cache的需要实现PSR规范的缓存接口, 默认使用 symfony/cache 的实现, 也可以自行替换为 Redis (需要实现 PSR-6 ),
     *
     * @param Option $option
     * @param LoggerInterface|null $logger
     * @param CacheItemPoolInterface|null $cache
     */
    public function __construct(
        Option                  $option,
        ?LoggerInterface        $logger = null,
        ?CacheItemPoolInterface $cache = null
    )
    {

        if (empty($option->getAppId())) {
            throw new RuntimeException("请完善配置信息中的App Key");
        }

        if (empty($option->getAppSecret())) {
            throw new RuntimeException("请完善配置信息中的App Secret");
        }

        $this->option = $option;

        $base_url = $option->getBaseUrl();

        if (empty($base_url)) {
            throw new RuntimeException("请检查配置信息中的Base Url");
        }

        if ($logger) {
            $this->logger = $logger;
        }

        $this->cache = new FilesystemAdapter();
        if ($cache) {
            $this->cache = $cache;
        }

        $this->client = new Client([
            'headers' => [
                "Content-Type" => 'application/json; charset=utf8',
            ],
            "base_uri" => $base_url,
            "timeout" => $this->option->getTimeOut(),
            "http_errors" => false,
            "debug" => $this->option->isDebug(),
        ]);
    }


    /**
     * 从缓存中获取accessToken 使用PSR-6缓存接口
     *
     * @return string
     * @throws CacheInvalidArgumentException
     * @throws TransferException
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    private function getAccessTokenFromCache(): string
    {
        // 过期时间为0时不使用缓存
        if ($this->option->getAuthExpires() == 0) {
            return $this->option->authorization()($this->client);
        }
        $cacheKey = sprintf(self::TOKEN_CACHE_KEY, $this->option->getAppId());

        $currentToken = $this->cache->getItem($cacheKey);
        if (!$currentToken->isHit()) {
            $token = $this->option->authorization()($this->client);
            $currentToken->set($token)->expiresAfter($this->option->getAuthExpires());
            $this->cache->save($currentToken);
        }

        return $currentToken->get();
    }

    /**
     * 发送SDK的请求
     *
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws ApplicationException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {

        $stack = HandlerStack::create();

        $this->lastRequestContext = [
            'request' => $request,
            "stage" => $this->option->getStage(),
        ];
        $start = microtime(true);
        // 应该使用的是 SDKRequest
        if ($request instanceof SdkRequest) {

            $this->lastRequestContext['request_id'] = $request->getUuid();
            $this->lastRequestContext['args'] = $request->getArgs();
            $this->lastRequestContext['endpoint'] = $request->getEndpoint();
            $this->lastRequestContext['action'] = $request->getAction();

            $start = $request->getStartAt();
            $middlewares = $request->getMiddlewares();
            foreach ($middlewares as $middleware) {
                $stack->push($middleware);
            }
            if ($request->authorization()) {
                try {
                    $jwtToken = $this->getAccessTokenFromCache();
                    $authMiddleware = $this->option->getAuthorizationMiddleware();
                    $stack->push(new $authMiddleware($jwtToken));
                } catch (CacheInvalidArgumentException $exception) {
                    // 缓存异常
                    throw  new ApplicationException("auth cache error " . $exception->getMessage());
                } catch (TransferException $exception) {
                    // 超时异常
                    if ($this->logger) {
                        $this->logger->error("sdk error, authorization timeout.");
                    }
                    throw new TimeOutExcetpion($request);
                } catch (RuntimeException|InvalidArgumentException $exception) {
                    // 参数异常
                    if ($this->logger) {
                        $this->logger->error("sdk error, cause by:" . $exception->getMessage());
                    }
                    throw new ApplicationException($exception->getMessage(), $request, [], $exception);
                }
            }
        }

        $options = [
            'handler' => $stack
        ];

        try {
            $response = $this->client->send($request, $options);
        } catch (TransferException $exception) {
            $this->lastRequestContext['expend'] = sprintf("%dms", microtime(true) * 1000 - $start * 1000);
            if ($this->logger) {
                $this->logger->error("time out!");
            }
            if (!($request instanceof SdkRequest)) {
                throw $exception;
            }
            throw new TimeOutExcetpion($request, $exception);
        } catch (GuzzleException $exception) {
            if ($this->logger) {
                $this->logger->error("sdk error, bad client!");
            }
            if (!($request instanceof SdkRequest)) {
                throw new ApplicationException($exception->getMessage(), null, [], $exception);
            }
            throw new ApplicationException($exception->getMessage(), $request, [], $exception);
        }

        $this->lastRequestContext['expend'] = sprintf("%dms", microtime(true) * 1000 - $start * 1000);
        $this->lastRequestContext['http_status'] = $response->getStatusCode();
        $this->lastRequestContext['response'] = $response;
        return $response;
    }

    /**
     * 解析SDK的响应结果，并处理异常
     *
     * @param SdkRequest $request
     * @param ResponseInterface $response
     * @return array<string, mixed>
     * @throws ApplicationException
     * @throws SdkException
     */
    public function resultDecode(SdkRequest $request, ResponseInterface $response): array
    {

        // context 是没有处理前的上下文
        $context = $this->lastRequestContext;

        if ($context['request']) {
            unset($context['request']);
        }
        if ($context['response']) {
            unset($context['response']);
        }

        if ($response->getStatusCode() >= 500) {
            if ($this->logger) {
                $this->logger->error("sdk error, Upstream service fail", $context);
            }
            throw new ApplicationException("SDK Upstream Failed", $request, []);
        }

        if ($response->getStatusCode() == 401) {
            if ($this->logger) {
                $this->logger->error("sdk error, auth fail", $context);
            }
            throw new ApplicationException("auth Failed", $request, []);
        }

        try {
            $rpcResult = $this->option->handlingPolicy()->process($response);
        } catch (GuiltyResultException $exception) {
            // 没有通过预期判定，转化为 SDK Exception
            $this->lastRequestContext['error_message'] = $exception->getMessage();
            $this->lastRequestContext['error_detail'] = $exception->getOptions(); // 获取错误的详情
            if ($this->logger) {
                $this->logger->warning("Error", $this->lastRequestContext);
            }
            throw new SdkException($exception->getMessage(), $request, $response, $exception->getOptions());
        } catch (UnknowResultException $exception) {
            // 无法解析，转化为 Application Exception
            // 日志中保留 OUT PUT
            $this->lastRequestContext['error_message'] = $exception->getMessage();
            $this->lastRequestContext['error_detail'] = $exception->getResult(); // 获取错误的详情
            if ($this->logger) {
                $this->logger->error("sdk error, bad response content.", $this->lastRequestContext);
            }
            throw new ApplicationException("Content Format error.", $request, []);
        }

        $version = $response->getHeader('ETag');
        if (!empty($version)) {
            $rpcResult = array_merge($rpcResult, [
                'version' => $version[0]
            ]);
        }

        if ($this->logger) {
            $this->logger->info("Success", $context);
        }
        return $rpcResult;
    }

    /**
     * 获取上一次请求的上下文
     *
     * @return array<string, mixed>
     */
    public function getLastRequestContext(): array
    {
        return $this->lastRequestContext;
    }
}
