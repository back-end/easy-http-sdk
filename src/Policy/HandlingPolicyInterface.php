<?php

namespace Jiwei\EasyHttpSdk\Policy;

use Psr\Http\Message\ResponseInterface;

interface HandlingPolicyInterface
{
    /**
     * @param ResponseInterface $response
     * @return array<string, mixed>
     */
    public function process(ResponseInterface $response): array;
}
