<?php

namespace Jiwei\EasyHttpSdk\Policy;

use Jiwei\EasyHttpSdk\Exception\GuiltyResultException;
use Jiwei\EasyHttpSdk\Exception\UnknowResultException;
use Psr\Http\Message\ResponseInterface;

class DefaultErrorHandlingPolicy implements HandlingPolicyInterface
{
    /**
     * 处理API层面异常的规则
     *
     * @param ResponseInterface $response
     * @return array<string, mixed>
     */
    public function process(ResponseInterface $response): array
    {
        $responseInfo = $response->getBody()->getContents();
        $rpcResult = json_decode($responseInfo, true);

        if (json_last_error()) {
            throw new UnknowResultException("Content Format error.", $responseInfo);
        }

        $status = $rpcResult['status'] ?? 1;
        if ($status != 0 && $response->getStatusCode() >= 400) {
            $errorMessage = sprintf("Api error : %s",$rpcResult['message'] ?? "");
            throw new GuiltyResultException($errorMessage, $rpcResult);
        }
        return $rpcResult;
    }
}
