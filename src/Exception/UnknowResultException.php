<?php

namespace Jiwei\EasyHttpSdk\Exception;

use Throwable;
use \UnexpectedValueException;

class UnknowResultException extends UnexpectedValueException
{
    /** @var string */
    public $result;

    /**
     * 无法解析API返回的异常，会转化为 Application Exception
     *
     * @param string $message
     * @param string $result
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = "",
        string $result = "",
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->result = $result;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }
}
