<?php

namespace Jiwei\EasyHttpSdk\Exception;

use GuzzleHttp\Exception\BadResponseException;
use Jiwei\EasyHttpSdk\Helper\QueryPath;
use Jiwei\EasyHttpSdk\Http\SdkRequest;
use Psr\Http\Message\ResponseInterface;
use Throwable;


class SdkException extends BadResponseException implements SdkExceptionInterface
{
    use QueryPath;

    /**
     * SDK 中的逻辑异常
     *
     * @param string $message
     * @param SdkRequest $request
     * @param ResponseInterface $response
     * @param array<string, mixed> $handlerContext
     * @param Throwable|null $previous
     */
    public function __construct(
        string            $message,
        SdkRequest        $request,
        ResponseInterface $response,
        array             $handlerContext = [],
        ?Throwable       $previous = null
    )
    {
        $this->endpoint = $request->getEndpoint();
        $this->action = $request->getAction();
        parent::__construct($message, $request, $response, $previous, $handlerContext);
    }

    /**
     * 获取上下文中错误集合
     *
     * @return array<string, mixed>
     */
    public function getSdkErrors(): array
    {
        return $this->getHandlerContext();
    }
}
