<?php

namespace Jiwei\EasyHttpSdk\Exception;

use Jiwei\EasyHttpSdk\Http\SdkRequest;
use Throwable;

class TimeOutExcetpion extends ApplicationException implements SdkExceptionInterface
{

    /** @var int 错误码 */
    protected $code = 408;

    /** @var string The error message */
    protected $message = "time out.";

    /**
     * 请求超时异常
     *
     * @param SdkRequest $request
     * @param Throwable|null $previous
     */
    public function __construct(
        SdkRequest  $request,
        ?Throwable $previous = null
    )
    {
        parent::__construct($this->message, $request, [], $previous);
    }
}
