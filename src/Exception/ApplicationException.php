<?php

namespace Jiwei\EasyHttpSdk\Exception;

use GuzzleHttp\Exception\ConnectException;
use Jiwei\EasyHttpSdk\Helper\QueryPath;
use Jiwei\EasyHttpSdk\Http\SdkRequest;
use Throwable;

class ApplicationException extends ConnectException implements SdkExceptionInterface
{
    use QueryPath;

    /**
     * 应用异常
     *
     * @param string $message
     * @param SdkRequest|null $request
     * @param array<string, mixed> $handlerContext
     * @param Throwable|null $previous
     */
    public function __construct(
        string      $message,
        SdkRequest  $request = null,
        array       $handlerContext = [],
        ?Throwable $previous = null
    ) {
        $this->endpoint = is_null($request) ? "" : $request->getEndpoint();
        $this->action = is_null($request) ? "" : $request->getAction();
        parent::__construct($message, $request, $previous, $handlerContext);
    }
}
