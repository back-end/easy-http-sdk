<?php

namespace Jiwei\EasyHttpSdk\Exception;

use Throwable;
use \UnexpectedValueException;

class GuiltyResultException extends UnexpectedValueException
{
    /** @var array<string, mixed> */
    private $options;

    /**
     * 判断为 API 逻辑错误的异常，会转化为 SDK Exception
     *
     * @param string $message
     * @param array<string, mixed> $options
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = "",
        array $options = [],
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->options = $options;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array<string, mixed>
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
