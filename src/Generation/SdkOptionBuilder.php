<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Generation;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class SdkOptionBuilder
{

    /** @var string 目标Endpoint目录 */
    private $endpointDstPath;

    /** @var string twig模板目录 */
    private $twigPath;

    /** @var Environment twig生成器 */
    protected $twig;

    /** @var string 包命名空间 */
    protected $package;

    /**
     * @param string $endpointDstPath
     * @param string $twigPath
     * @param string $package
     */
    public function __construct(string $endpointDstPath, string $twigPath, string $package)
    {
        $this->endpointDstPath = $endpointDstPath;
        $this->twigPath = $twigPath;
        $this->package = $package;

        $loader = new FilesystemLoader($this->twigPath);
        $this->twig = new Environment($loader);
    }

    /**
     * 根据配置创建Endpoint类
     *
     * @return void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function build(): void
    {
        $phpCode = $this->twig->render('Option.temp', [
            "Name" => $this->package
        ]);
        file_put_contents($this->endpointDstPath . $this->package . "Option.php", $phpCode);
    }

}
