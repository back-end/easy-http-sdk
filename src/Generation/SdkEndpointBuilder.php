<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Generation;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use Yosymfony\Toml\Toml;

class SdkEndpointBuilder
{

    /** @var string Toml目录 */
    private $endpointSrcPath;

    /** @var string 目标Endpoint目录 */
    private $endpointDstPath;

    /** @var string twig模板目录 */
    private $twigPath;

    /** @var array<string, mixed> 读取的配置 */
    protected $tomlSetting = [];

    /** @var Environment twig生成器 */
    protected $twig;

    /** @var string 包命名空间 */
    protected $package;

    /** @var string PHP适配版本 70000 80000 */
    protected $version;

    /**
     * @param string $endpointDstPath
     * @param string $endpointSrcPath
     * @param string $twigPath
     * @param string $package
     * @param string $version
     */
    public function __construct(string $endpointDstPath, string $endpointSrcPath, string $twigPath, string $package, string $version = "70000")
    {
        $this->endpointDstPath = $endpointDstPath;
        $this->endpointSrcPath = $endpointSrcPath;
        $this->twigPath = $twigPath;
        $this->package = $package;
        $this->version = $version;

        $loader = new FilesystemLoader($this->twigPath);
        $this->twig = new Environment($loader);
    }

    /**
     * 加载目录
     *
     * @return void
     */
    private function LoadToml(): void
    {
        $dh = opendir($this->endpointSrcPath);
        if (!$dh) {
            throw new \RuntimeException(sprintf("src path [%s] can not open ", $this->endpointSrcPath));
        }
        while (($file = readdir($dh)) !== false) {

            if (in_array($file, [".", ".."])) {
                continue;
            }
            $actions = [];
            if (str_contains($file, ".toml")) {
                $actions = Toml::ParseFile($this->endpointSrcPath . $file);
            }
            $endpointName = ucfirst(str_replace(".toml", "", $file));

            $this->tomlSetting[$endpointName] = $actions;
        }
        closedir($dh);
    }

    /**
     * 根据配置创建Endpoint类
     *
     * @return int
     */
    public function build(): int
    {
        $this->LoadToml();

        $renderResult = [];

        foreach ($this->tomlSetting as $name => $endpoints) {


            $endpointFunc = [];
            foreach ($endpoints as $func => $endpoint) {
                $description = $endpoint["description"] ?? $func;
                $uri = $endpoint['endpoint'] ?? "";
                if (empty($uri)) {
                    throw new \RuntimeException(sprintf("endpoint [%s] method %s required a uri ", $name, $func));
                }
                $method = $endpoint['method'] ?? "";
                if (empty($method)) {
                    throw new \RuntimeException(sprintf("endpoint [%s] method %s required a http method ", $name, $func));
                }
                $query = $endpoint['query'] ?? [];
                $body = $endpoint['body'] ?? false;
                $cache = $endpoint['cache'] ?? false;
                $auth = $endpoint['auth'] ?? false;

                $args = "";
                $params = [];

                $middlewareRuleCode = "";

                if ($auth) {
                    $middlewareRuleCode .= '->withAuthorization()';
                }

                $require_regex = '~{([\w.]+)}~s';

                $matched = preg_match_all($require_regex, $uri, $matches);

                // 检测重复元素
                if ($matched != count(array_unique($matches[1]))){
                    throw new \RuntimeException(sprintf("endpoint [%s] method %s required has same args ", $name, $func));
                }

                if ($matched > 0) {
                    foreach ($matches[1] as $item) {
                        $args .= 'string $' . $item . ', ';
                        $params[] = [
                            "type" => "string",
                            "name" => '$' . $item,
                        ];
                        if (!str_contains($uri, "{$item}")) {
                            throw new \RuntimeException(sprintf("endpoint [%s] method %s required argument not found in uri ", $name, $func));
                        }
                        $uri = str_replace("{" . $item . "}", '$' . $item , $uri);
                    }
                }

                if ($cache) {
                    $args .= '?string $version = "", ';
                    $params[] = [
                        "type" => "string|null",
                        "name" => '$version',
                    ];
                    $middlewareRuleCode .= '->withEtag($version)';
                }

                $queryJoin = [];
                if (count($query) != count(array_unique($query))){
                    throw new \RuntimeException(sprintf("endpoint [%s] method %s query has same args ", $name, $func));
                }
                foreach ($query as $item) {
                    $args .= '?string $' . $item . ' = "", ';
                    $params[] = [
                        "type" => "string|null",
                        "name" => '$' . $item,
                    ];
                    $queryJoin[] = [
                        "name" => "$" . $item,
                        "query" => sprintf('"%s=%s&"', $item, "$" . $item),
                    ];
                }

                $bodyStr = "";
                if ($body) {
                    $args .= '?array $body = [], ';
                    $bodyStr = ', json_encode($body)';
                    $params[] = [
                        "type" => "array<string, mixed>|null",
                        "name" => '$body',
                    ];
                }

                $currentFunc = [
                    "Action" => $func,
                    "Description" => $description,
                    "Method" => $method,
                    "Uri" => $uri,
                    "Body" => $bodyStr,
                    "Args" => $args,
                    "Params" => $params,
                    "Middleware" => $middlewareRuleCode,
                    "Join" => $queryJoin,
                ];
                $endpointFunc[] = $currentFunc;
            }
            $endpointSchema = [
                "Endpoint" => $name,
                "Package" => $this->package,
                "Func" => $endpointFunc,
            ];
            try {
                $phpCode = $this->twig->render(sprintf('Endpoint%s.temp', $this->version), $endpointSchema);
                $renderResult[$name] = $phpCode;
                echo sprintf("Render Class[%s] ...", $name) . PHP_EOL;
            } catch (LoaderError|RuntimeError|SyntaxError $e) {
                echo sprintf("Render Error : %s", $e->getRawMessage()) . PHP_EOL;
            }
        }

        foreach ($renderResult as $name => $phpCode) {
            file_put_contents($this->endpointDstPath . $name . ".php", $phpCode);
            echo sprintf("Save Class[%s] ...", $name) . PHP_EOL;
        }

        return count($this->tomlSetting);
    }

}
