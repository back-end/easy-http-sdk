<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Middleware\Auth;

use Psr\Http\Message\RequestInterface;

class JwtMiddleware implements AuthMiddlewareInterface
{
    /** @var string jwtToken */
    protected $token;

    public function __construct(string $token = ""){
        $this->token = $token;
    }

    /**
     * 为请求添加Jwt Token的中间件
     *
     * @param callable $handler
     * @return callable
     */
    public function __invoke(callable $handler): callable
    {
        return function (
            RequestInterface $request,
            array $options
        ) use ($handler) {
            $request = $request->withHeader("Authorization", "Bearer " . $this->token );
            return $handler($request, $options);
        };
    }
}
