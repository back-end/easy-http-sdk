<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Middleware\Auth;

use Jiwei\EasyHttpSdk\Middleware\MiddlewareInterface;

interface AuthMiddlewareInterface extends MiddlewareInterface
{
    /**
     * @param string $token
     */
    public function __construct(string $token = "");
}
