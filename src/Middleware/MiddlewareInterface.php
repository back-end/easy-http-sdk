<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Middleware;

interface MiddlewareInterface
{
    /**
     * @param callable $handler
     * @return callable
     */
    public function __invoke(callable $handler): callable;
}
