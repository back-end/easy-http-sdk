<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Middleware;

use Psr\Http\Message\RequestInterface;

class RequestIDMiddleware implements MiddlewareInterface
{
    /** @var string UUID */
    protected $uuid;

    /**
     * @param string $uuid
     */
    public function __construct(string $uuid = "")
    {
        $this->uuid = $uuid;
    }

    /**
     * 为请求添加ID的中间件
     *
     * @param callable $handler
     * @return callable
     */
    public function __invoke(callable $handler): callable
    {
        return function (
            RequestInterface $request,
            array            $options
        ) use ($handler) {
            $request = $request->withHeader("X-Request-ID", $this->uuid);
            return $handler($request, $options);
        };
    }
}
