<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Middleware;

use Psr\Http\Message\RequestInterface;

class EtagMiddleware implements MiddlewareInterface
{
    /** @var string Version */
    private $version;

    /**
     * @param string $version
     */
    public function __construct(string $version)
    {
        $this->version = $version;
    }

    /**
     * 为请求添加Etag的中间件
     *
     * @param callable $handler
     * @return callable
     */
    public function __invoke(callable $handler): callable
    {
        return function (
            RequestInterface $request,
            array            $options
        ) use ($handler) {
            if ($this->version !== "") {
                $ETagHeaderAttribute = "If-Match";
                if ($request->getMethod() == "GET") {
                    $ETagHeaderAttribute = "If-None-Match";
                }
                $request = $request->withHeader($ETagHeaderAttribute, $this->version);
            }
            return $handler($request, $options);
        };
    }
}
