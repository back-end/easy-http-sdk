<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Http;

use ArrayAccess;
use GuzzleHttp\Psr7\Response;
use Jiwei\EasyHttpSdk\Exception\SdkException;

/**
 * @implements ArrayAccess<string, mixed>
 */
class Context implements ArrayAccess
{

    /** @var SdkRequest */
    public $request;

    /** @var Response */
    public $response;

    /** @var int  */
    public $requestId;

    /** @var string  */
    public $endpoint;

    /** @var string  */
    public $action;

    /** @var string  */
    public $stage;

    /** @var string  */
    public $expend;

    /** @var string  */
    public $httpStatus;

    /** @var array<string, mixed>  */
    public $data;

    /** @var SdkException  */
    public $exception;

    /** @var string  */
    public $errorMessage;

    /** @var array<string, mixed>|string  */
    public $errorDetail;

    /**
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->$offset);
    }

    /**
     * @param $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->$offset ?? null;
    }

    /**
     * @param $offset
     * @param $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (in_array($offset, get_object_vars($this))) {
            $this->$offset = $value;
        }
    }

    /**
     * @param $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->$offset = null;
    }
}
