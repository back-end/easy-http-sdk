<?php
declare(strict_types=1);

namespace Jiwei\EasyHttpSdk\Http;

use GuzzleHttp\Psr7\Request;
use Jiwei\EasyHttpSdk\Middleware\EtagMiddleware;
use Jiwei\EasyHttpSdk\Middleware\MiddlewareInterface;
use Jiwei\EasyHttpSdk\Middleware\RequestIDMiddleware;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

class SdkRequest extends Request
{

    /** @var string SDK uuid */
    private $uuid;

    /** @var string SDK Endpoint */
    private $endpoint;

    /** @var string SDK方法 */
    private $action;

    /** @var float 开始时间 */
    private $startAt;

    /** @var array<string, mixed> 调用参数列表 */
    private $args;

    /** @var MiddlewareInterface[] Guzzle中间件列表 */
    private $middlewares = [];

    /** @var bool 鉴权开关 */
    private $authorization = false;

    /**
     * Construct of SdkRequest style by psr-7
     *
     * @param string $method HTTP method
     * @param string|UriInterface $uri URI
     * @param string $endpoint Endponit
     * @param string $action  action
     * @param array<string, string|string[]> $headers Request headers
     * @param string|resource|StreamInterface|null $body Request body
     * @param string $version Protocol version
     */
    public function __construct(
        string $method,
               $uri,
               $endpoint,
               $action,
        array  $headers = [],
               $body = null,
        string $version = '1.1'
    )
    {
        parent::__construct($method, $uri, $headers, $body, $version);
        $this->startAt = microtime(true);
        $this->endpoint = $endpoint;
        $this->action = $action;
    }


    /**
     * 获取本次请求的方法参数
     *
     * @return array<string, mixed>
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * 设置本次请求的方法参数
     *
     * @param array<string, mixed> $args
     * @return SdkRequest
     */
    public function withArgs(array $args): self
    {
        $this->args = $args;
        return $this;
    }

    /**
     * 获取本次请求需要使用的中间件
     *
     * @return MiddlewareInterface[]
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    /**
     * 获取 SDK Endpoint
     *
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * 获取 SDK Action
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * 判断鉴权
     *
     * @return bool
     */
    public function authorization(): bool
    {
        return $this->authorization;
    }

    /**
     * 开启鉴权
     *
     * @return SdkRequest
     */
    public function withAuthorization(): self
    {
        $this->authorization = true;
        return $this;
    }

    /**
     * 使用ETAG中间件来传递version
     *
     * @param string $version
     * @return SdkRequest
     */
    public function withEtag(string $version): self
    {
        $EtgMiddleware = new EtagMiddleware($version);
        $this->middlewares[] = $EtgMiddleware;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * 使用 RequestID 中间件
     *
     * @param string $uuid
     * @return SdkRequest
     */
    public function withRequestID(string $uuid = ""): self
    {
        if (empty($uuid)) {
            $uuid = sprintf('%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff));
        }
        $this->uuid = $uuid;
        $RequestIDMiddleware = new RequestIDMiddleware($this->uuid);
        $this->middlewares[] = $RequestIDMiddleware;
        return $this;
    }

    /**
     * 获取初始化时间
     *
     * @return float
     */
    public function getStartAt(): float
    {
        return $this->startAt;
    }

}
