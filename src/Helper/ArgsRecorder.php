<?php

namespace Jiwei\EasyHttpSdk\Helper;

trait ArgsRecorder
{

    /**
     * @param string $func
     * @param array $params
     * @return array
     */
    private function ArgsRecoder(string $func, array $params): array
    {
        $result = [];
        try {
            $reflectionFunction = new \ReflectionMethod($func);
            foreach ($reflectionFunction->getParameters() as $key => $parameter) {
                if (in_array($key, ['then', 'catch'])) {
                    continue;
                }
                $result[$parameter->name] = $params[$key] ?? null;
            }
        }catch (\ReflectionException $exception) {
            throw new \RuntimeException("Refection get args error.");
        }
        return $result;
    }
}
