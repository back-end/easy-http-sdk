<?php

namespace  Jiwei\EasyHttpSdk\Helper;

use Closure;
use  Jiwei\EasyHttpSdk\Exception\SdkException;

trait CallBackHelper
{

    /**
     * @var SdkException|null
     */
    private $lastException = null;

    /**
     * @var mixed
     */
    private $lastResult = [];

    /**
     * @return mixed
     */
    public function getLastResult()
    {
        return $this->lastResult;
    }

    /**
     * @param mixed $lastResult
     */
    protected function setLastResult($lastResult): void
    {
        $this->lastResult = $lastResult;
    }

    /**
     * @return SdkException|null
     */
    public function getLastException(): ?SdkException
    {
        return $this->lastException;
    }

    /**
     * @param SdkException $lastException
     */
    protected function setLastException(SdkException $lastException): void
    {
        $this->lastException = $lastException;
    }

    /**
     * @return Closure
     */
    private function defaultSuccessCallBack(): Closure
    {
        // 这里使用默认作用域
        return function () {
            return $this->getLastResult();
        };
    }

    /**
     * @return Closure
     */
    private function defaultFailedCallBack(): Closure
    {
        // 这里使用默认作用域
        return function () {
            throw $this->getLastException();
        };
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return array_merge($this->application->getLastRequestContext(), [
            'data' => $this->getLastResult(),
            'exception' => $this->getLastException(),
        ]);
    }


}
