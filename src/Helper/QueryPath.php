<?php

namespace Jiwei\EasyHttpSdk\Helper;

/**
 *  用于扩展错误类型
 */
trait QueryPath
{
    /** @var string Endpoint */
    protected $endpoint;

    /** @var string Endpoint */
    protected $action;

    /**
     * 获取错误详情位置
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->endpoint . ":" . $this->action;
    }
}
