<?php

use Jiwei\EasyHttpSdk\Option;
use PHPUnit\Framework\TestCase;

final class OptionTest extends TestCase
{

    protected mixed $option = null;

    /**
     * 测试Option实现实例的初始化（以匿名函数形式）
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->option = new class () extends Option {
            /** @var string API鉴权地址  */
            private const AUTH_API_ROUTE = "/api/access/token";

            /** @var int API鉴权过期时间 */
            const AUTH_CACHE_EXPIRES_AT = 2400;

            /** @var array<string, string> API HOST */
            const ENDPOINT_HOSTS = [
                "local" => "127.0.0.1",
                "development" => "dev.test.com",
                "production" => "pro.test.com",
            ];

            public function authorization(): Closure
            {
                return function () {
                    echo sprintf("根据Auth API[%s]获取了TOKEN", self::AUTH_API_ROUTE);
                    return "i am a token";
                };
            }
        };
    }

    /**
     * 测试
     *
     * @return void
     */
    public function testOptionDefaultDebug()
    {
        $checkDebug = $this->option->isDebug();
        $this->assertEquals($checkDebug, false, "默认的 debug 级别应该是 false ");
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testOptionSetUndefineStage()
    {
        $defaultStage = $this->option->getStage();
        $this->assertEquals($defaultStage, "development", "默认的 stage 应该是 development ");
        $this->expectException(InvalidArgumentException::class);
        $this->option->setStage("WoooHaa");
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testOptionBaseUrl()
    {
        $defaultUrl = $this->option->getBaseUrl();
        $this->assertEquals($defaultUrl, "dev.test.com", "默认的 stage 应该是 development,所以默认的 url 是 devtest");
        $this->option->setStage("production");
        $proUrl = $this->option->getBaseUrl();
        $this->assertEquals($proUrl, "pro.test.com", "设置 Pro stage 应该可以正常显示");
    }
}
